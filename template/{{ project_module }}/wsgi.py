"""
WSGI config for {{ project_name }}.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

{%- if respect_x_forward_header %}
from proxyprefix.wsgi import ReverseProxiedApp

{%- endif %}

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{{ project_module }}.settings")

{% if respect_x_forward_header %}
application = ReverseProxiedApp(get_wsgi_application())
{% else %}
application = get_wsgi_application()
{% endif -%}
