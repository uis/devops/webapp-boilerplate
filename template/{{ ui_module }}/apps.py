from django.apps import AppConfig


class Config(AppConfig):
    name = "{{ ui_module }}"
    verbose_name = "{{ ui_name }}"
