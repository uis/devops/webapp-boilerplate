from django.apps import AppConfig


class Config(AppConfig):
    name = "{{ api_module }}"
    verbose_name = "{{ api_name }}"
