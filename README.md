# Django Boilerplate

This repository contains the [copier](https://github.com/copier-org/copier) template for Django
projects created by the Automation Group.

## Quickstart

**Note** - The copier command will ask a bunch of questions (prompts on the command line). Use the defaults
if running for the first time or see the Configuration section below for answers to those questions.

Install pipx following [the instructions on their site](https://pypa.github.io/pipx/).

```bash
$ pipx install copier poetry poethepoet
$ pipx inject poetry poethepoet
$ copier copy --trust --vcs-ref <TAG> https://gitlab.developers.cam.ac.uk/uis/devops/webapp-boilerplate.git <OUTPUT_PATH>
$ cd boilerplater_webapp
$ cp secrets.env.in secrets.env
$ poe up --wait
```

Where:
- `<TAG>` is a version of the template (e.g. 1.0.0)
- `<OUTPUT_PATH>` is a path where generated content will be stored. Can be relative or absolute.

For further information please see
[the DevOps Division Guidebook entry on "Webapp Developer Environment"](https://guidebook.devops.uis.cam.ac.uk/en/latest/notes/webapp-dev-environment/)

## Configuration

Copier will ask you for the following values:

* **project_name**: A human-readable name for the project. E.g. "UIS
    Frobnication-Requester".
* **project_slug**: A filename-safe version of the project name. Used to
    construct filenames for project metadata. By default, generated from project
    name. E.g., for the project name above, "uis_frobnication-requester".
* **project_repo**: Full URL of the repository of the project.
* **project_module**: Name of the Python module containing project-wide
    settings, URL mapping, etc. By default, this is generated from project name,
    E.g., for the project name above, "uis_frobnication_requester_project".
* **application_name**: A human-readable name for the project's first Django
    application. By default, this is equal to the project name.
* **application_slug**: A filename-safe version of the application name. Used to
    construct filenames and the Django view namespace for the application.
    E.g., for the project name above, "uis_frobnication-requester".
* **application_module**: Name of the Python module containing the first
    application for the project. By default, this is generated from the project
    name.  E.g., for the project name above,
    "uis_frobnication_requester".
* **include_ui**: Whether a UI is included. If no UI is included, this is a pure
    API application.
* **ui_name**: A human-readable name for the project's UI Django application.
    By default, this is the project's name suffixed with "UI".
* **ui_slug**: A filename-safe name for the UI application. By default, just "ui".
* **ui_module**: Name of the Python module containing the UI application. By
    default, the same as the ui_slug.
* **api_name**: A human-readable name for the project's API Django application.
    By default, this is the project's name suffixed with "API".
* **api_slug**: A filename-safe name for the API application. By default, just
    "api".
* **api_module**: Name of the Python module containing the API application. By
    default, the same as the api_slug.
* **secret_key**: Secret key used in development. By default this is a long
    string made unique by prepending the **project_slug** value.
* **respect_x_forward_header**: Whether the ``X-Forwarded-...`` family of
    headers should be respected. For deployments behind an nginx or GCP incress,
    this option should be "YES".

### "Slug" versus "module"

It's worth distinguishing between "slug" and "module" names here. The practical
difference is that a "slug" name may contain a hyphen and a "module" name may
not. The semantic difference is that the "module" name is used when one needs to
refer to a particular bit of code and "slug" is used for various "short name"
metadata. The difference is best demonstrated by the project slug/module name
and application slug/module name. Often the name of the first application is
better represented by the project name and the Django project has to have a
similar but different name. Having the project module name explicitly end with
``_project`` makes the distinction clear.


## Testing

The repository is tested via the `tox` test runner. The tests include a basic
check that the template can generate output and that running tox in the
generated output succeeds. Before running tox in the generated output, `npm` is
installed in order to be able to run `npm install` and `npm run build` to
produce a React frontend. Therefore allowing the UI `IndexTests` test to pass.

## Renovate Presets

This repository also includes renovate presets specific to the webapp boilerplate.

### Group Python updates

The [groupPythonVersionUpdates](groupPythonVersionUpdates.json) preset will group all updates for
the python language version.

```js
{
  extends: [
    // ... other presets ...
    "local>uis/devops/webapp-boilerplate:groupPythonVersionUpdates"
  ]
}
```

### Keeping Python version fresh in CI

The [pythonVersionCIManager](pythonVersionCIManager.json) preset will ensure that the python
version specified in any `.gitlab-ci.yml` or `webapp.gitlab-ci.yml` file is kept up to date.

Example of the expected python version within `.gitlab-ci.yml` and `webapp.gitlab-ci.yml`.

```yml
  parallel:
    matrix:
      - PYTHON_VERSION: "3.13" # Make sure this matches the version in pyproject.toml and the Dockerfile.
```

Example `renovate.json`:

```js
{
  extends: [
    // ... other presets ...
    "local>uis/devops/webapp-boilerplate:pythonVersionCIManager"
  ]
}
```

