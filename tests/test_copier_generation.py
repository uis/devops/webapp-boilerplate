import re
import os
import sh
import sys
import pytest
import copier
from binaryornot.check import is_binary

PATTERN = b"{{([^{}]+)}}"
RE_OBJ = re.compile(PATTERN)


@pytest.fixture(params=[
        {
            "respect_x_forward_header": True,
            "include_ui": True,
            "include_api": True,
            "dst": "/tmp/include_all"
        },
        {
            "respect_x_forward_header": True,
            "include_ui": True,
            "include_api": False,
            "dst": "/tmp/include_ui"
        },
        {
            "respect_x_forward_header": True,
            "include_ui": False,
            "include_api": True,
            "dst": "/tmp/include_api"
        },
        {
            "respect_x_forward_header": False,
            "include_ui": False,
            "include_api": False,
            "dst": "/tmp/include_none"
        },
])
def context(request):
    "Context for copier parametrised with common variants."
    return {
        "project_name": "Test Project",
        **request.param
    }


def test_rendered_project(context):
    """Generate project from template using context."""
    generated = copier.Worker(
        src_path=".",
        dst_path=context["dst"],
        defaults=True,
        unsafe=True,
        data=context
    )
    generated.run_copy()
    assert os.path.exists(f"{context['dst']}/.copier-answers.yml")


def build_files_list(root_dir):
    """Build a list containing absolute paths to the generated files."""
    return [
        os.path.join(dirpath, file_path)
        for dirpath, subdirs, files in os.walk(root_dir)
        for file_path in files
    ]


def check_paths(paths):
    """Method to check all paths have correct substitutions,
    used by other tests cases
    """
    # Assert that no match is found in any of the files
    for path in paths:
        if is_binary(path):
            continue

        for line in open(path, "rb"):
            match = RE_OBJ.search(line)
            msg = "copier variable not replaced in {}"
            assert match is None, msg.format(path)


def test_no_dangling_variables(context):
    """generated project should have no copier variables left"""
    paths = build_files_list(context["dst"])
    assert paths
    check_paths(paths)


def test_tox(context):
    """generated project should pass the tox tests"""
    # Attempt to sanitise the environment so that tox does not get confused if
    # it is running within another tox.
    tox_test_env = {
        k: v for k, v in os.environ.items()
        if k not in [
                'PIP_DISABLE_PIP_VERSION_CHECK',
                'TOX_ENV_NAME',
                'TOX_WORK_DIR',
                'TOX_ENV_DIR',
                'VIRTUAL_ENV',
                'PYTEST_CURRENT_TEST',
                'DJANGO_FRONTEND_APP_BUILD_DIR'
        ]
    }
    sh.tox(
        _cwd=str(context["dst"]),
        _err_to_out=True,
        _out=sys.stdout,
        _env=tox_test_env,
    )


def test_pre_commit(context):
    """generated project should pass pre-commit checks"""
    sh.poetry("install", _cwd=context["dst"])
    sh.poetry(
        "run", "pre-commit", "run", "--all-files",
        _cwd=context["dst"],
        _err_to_out=True,
        _out=sys.stdout,
    )
